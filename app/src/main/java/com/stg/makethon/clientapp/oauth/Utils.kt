package com.stg.makethon.clientapp.oauth

import android.content.Context
import com.squareup.moshi.Moshi
import timber.log.Timber
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

object Utils {
    private const val INITIAL_READ_BUFFER_SIZE = 1024

    fun loadConfig(context: Context): AuthConfig? {
        try {
            val moshiClient = Moshi.Builder().build()
            val adapter = moshiClient.adapter(AuthConfig::class.java)
            val inStream = context.assets.open("auth.json")
            val confString = readInputStream(inStream)
            val conf = adapter.fromJson(confString)
            Timber.d(conf.toString())
            return conf
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    private fun readInputStream(inStream: InputStream): String {
        val br = BufferedReader(InputStreamReader(inStream))
        val buffer = CharArray(INITIAL_READ_BUFFER_SIZE)
        val sb = StringBuilder()
        var readCount = br.read(buffer)
        while (readCount != -1) {
            sb.append(buffer, 0, readCount)
            readCount = br.read(buffer)
        }
        return sb.toString()
    }
}