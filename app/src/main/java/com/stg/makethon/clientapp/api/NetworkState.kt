package com.stg.makethon.clientapp.api

class NetworkState internal constructor(val status: Status, val msg: String) {

    enum class Status {
        RUNNING,
        SUCCESS,
        FAILED
    }

    companion object {

        val LOADED: NetworkState = NetworkState(Status.SUCCESS, "Success")
        internal val LOADING: NetworkState = NetworkState(Status.RUNNING, "Running")

    }
}