package com.stg.makethon.clientapp.model

data class UserData(val uuid: String, val expiry: Long)

data class Bet(
    val horseName: String,
    val raceName: String,
    val date: String,
    val betValue: Double,
    val returns: Double,
    val collected: Boolean
)

data class UserDetailsEnv(val userDetails: UserDetails)

data class UserDetails(
    val id: String,
    val name: String,
    val bets_processed: List<ProcessedBet>,
    val bets_placed: List<PlacedBet>
)

data class ProcessedBet(
    val placed: Double,
    val won: Double,
    val success: Boolean,
    val horse_id: Int,
    val horse_name: String,
    val race_name: String
)

data class PlacedBet(
    val placed: Double,
    val horse_id: Int,
    val horse_name: String,
    val race_name: String
)