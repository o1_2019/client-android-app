package com.stg.makethon.clientapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.stg.makethon.clientapp.api.NetworkState
import com.stg.makethon.clientapp.api.ServiceGenerator
import com.stg.makethon.clientapp.api.Urls
import com.stg.makethon.clientapp.api.UserService
import com.stg.makethon.clientapp.model.UserDetailsEnv
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.concurrent.Executors

class MainActivityViewModel(app: Application) : AndroidViewModel(app), Callback<UserDetailsEnv> {
    private val networkStateLiveData = MutableLiveData<NetworkState>()
    private val userDetailsLiveData = MutableLiveData<UserDetailsEnv>()

    private val userService by lazy {
        ServiceGenerator.getInstance(Urls.BASE_URL).create(UserService::class.java)
    }

    private val executor by lazy { Executors.newSingleThreadExecutor() }

    fun getUserDetails() {
        executor.execute { fetchUserDetails() }
    }

    private fun fetchUserDetails() {
        networkStateLiveData.postValue(NetworkState.LOADING)
        userService.getUserDetails("dbsheta@gmail.com").enqueue(this)
    }

    override fun onFailure(call: Call<UserDetailsEnv>, t: Throwable) {
        Timber.e(t)
        networkStateLiveData.postValue(NetworkState(NetworkState.Status.FAILED, t.localizedMessage))
    }

    override fun onResponse(call: Call<UserDetailsEnv>, response: Response<UserDetailsEnv>) {
        if (response.isSuccessful) {
            networkStateLiveData.postValue(NetworkState.LOADED)
            userDetailsLiveData.postValue(response.body())
        } else {
            networkStateLiveData.postValue(NetworkState(NetworkState.Status.FAILED, response.errorBody()?.string()!!))
        }
    }

    fun networkState(): LiveData<NetworkState> = networkStateLiveData
    fun userDetails(): LiveData<UserDetailsEnv> = userDetailsLiveData
}