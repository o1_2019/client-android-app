package com.stg.makethon.clientapp.ui.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.enums.Align
import com.anychart.enums.LegendLayout
import com.stg.makethon.clientapp.R
import com.stg.makethon.clientapp.model.UserDetails


/**
 * A simple [Fragment] subclass.
 *
 */
class PieChartFragment : Fragment() {
    private lateinit var userDetails: UserDetails
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_pie_chart, container, false)
        setupChart(view)
        return view
    }

    companion object {
        fun newInstance(ud: UserDetails): PieChartFragment {
            val pieChartFragment = PieChartFragment()
            pieChartFragment.userDetails = ud
            return pieChartFragment
        }
    }

    private fun setupChart(view: View) {
        val pie = AnyChart.pie()
        val dataPoints = ArrayList<DataEntry>()
        val wins = userDetails.bets_processed.map { it.success }.filter { it }.size
        val losses = userDetails.bets_processed.size - wins
        dataPoints.add(ValueDataEntry("Win", wins))
        dataPoints.add(ValueDataEntry("Loss", losses))
        pie.data(dataPoints)
        pie.title("Win Loss Analysis")
        pie.labels().position("inside")
        pie.legend()
            .position("center-bottom")
            .itemsLayout(LegendLayout.HORIZONTAL)
            .align(Align.CENTER)
        val chart = view.findViewById<AnyChartView>(R.id.pieChart)
        chart.setChart(pie)
    }
}
