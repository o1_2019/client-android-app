package com.stg.makethon.clientapp.ui

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.awesome.shorty.AwesomeToast
import com.shashank.sony.fancydialoglib.FancyAlertDialog
import com.shashank.sony.fancydialoglib.Icon
import com.stg.makethon.clientapp.R
import com.stg.makethon.clientapp.api.NetworkState
import com.stg.makethon.clientapp.model.Bet
import com.stg.makethon.clientapp.model.ProcessedBet
import com.stg.makethon.clientapp.model.UserDetails
import com.stg.makethon.clientapp.ui.adapters.BetsAdapter
import com.stg.makethon.clientapp.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_previous_bets.*
import timber.log.Timber

class PreviousBetsActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {
    private lateinit var userDetails: UserDetails
    private lateinit var betsAdapter: BetsAdapter
    private val viewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }
    private var bets = arrayListOf<Bet>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_previous_bets)
        setupRecyclerView()
        swipeRefresh.setOnRefreshListener(this)
        setupViewModel()
        viewModel.getUserDetails()
    }

    private fun setupViewModel() {
        viewModel.userDetails().observe(this, Observer {
            if (it != null) {
                Timber.d(it.userDetails.toString())
                userDetails = it.userDetails
                bets = getBets(userDetails.bets_processed)
                betsAdapter.updateList(bets)
                betsAdapter.notifyDataSetChanged()
            }
        })

        viewModel.networkState().observe(this, Observer {
            when (it) {
                NetworkState.LOADING -> swipeRefresh.isRefreshing = true
                else -> swipeRefresh.isRefreshing = false
            }
        })
    }

    private fun setupRecyclerView() {
        betsAdapter = BetsAdapter(this, bets) {
            when {
                it.returns > 0 && !it.collected -> showDialog(it)
                it.returns > 0 -> AwesomeToast.info(
                    applicationContext,
                    "You have already collected your reward!",
                    Toast.LENGTH_LONG
                ).show()
                else -> AwesomeToast.error(
                    applicationContext,
                    "Sorry, you lost this bet!",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        prevBetsList.layoutManager = LinearLayoutManager(this)
        prevBetsList.adapter = betsAdapter
    }

    private fun showDialog(bet: Bet) {
        FancyAlertDialog.Builder(this)
            .setTitle("Collect Returns")
            .setMessage("You won ${getString(R.string.currency)} ${bet.returns} from this bet. Do you want to transfer it to your bank account?")
            .setIcon(R.drawable.ic_info, Icon.Visible)
            .setPositiveBtnText("Yes")
            .setNegativeBtnText("No")
            .isCancellable(false)
            .OnPositiveClicked {
                AwesomeToast.success(
                    applicationContext,
                    "You will receive ${getString(R.string.currency)} ${bet.returns} in your bank account in 4 working days",
                    Toast.LENGTH_LONG
                ).show()
                val pos = bets.lastIndexOf(bet)
                bets.removeAt(pos)
                bets.add(pos, Bet(bet.horseName, bet.raceName, bet.date, bet.betValue, bet.returns, true))
                betsAdapter.updateList(bets)
                betsAdapter.notifyDataSetChanged()
            }
            .OnNegativeClicked {}
            .build()
    }

    override fun onRefresh() {
        viewModel.getUserDetails()
    }

    private fun getBets(listBets: List<ProcessedBet>): ArrayList<Bet> {
        val bets = ArrayList<Bet>()
        var i = 1
        for (bet: ProcessedBet in listBets) {
            bets.add(Bet(bet.horse_name, bet.race_name, "${1 + i}/03/2019", bet.placed, bet.won, false))
            i += 1
        }
        return bets
    }
}
