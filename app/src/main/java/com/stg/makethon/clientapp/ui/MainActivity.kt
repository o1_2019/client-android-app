package com.stg.makethon.clientapp.ui

import android.Manifest
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.stg.makethon.clientapp.R
import com.stg.makethon.clientapp.api.NetworkState
import com.stg.makethon.clientapp.model.PlacedBet
import com.stg.makethon.clientapp.model.UserDetails
import com.stg.makethon.clientapp.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bet_placed.view.*
import kotlinx.android.synthetic.main.dashboard.*
import kotlinx.android.synthetic.main.dashboard.view.*
import org.jetbrains.anko.intentFor
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import timber.log.Timber


class MainActivity : AppCompatActivity(), View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    lateinit var userDetails: UserDetails
    private val RC_BLUETOOTH = 900
    private val viewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupButtons()
        getBluetoothPermission()
        swipeRefresh.setOnRefreshListener(this)
        setupViewModel()
        viewModel.getUserDetails()
    }

    private fun setupViewModel() {
        viewModel.userDetails().observe(this, Observer {
            if (it != null) {
                Timber.d(it.userDetails.toString())
                userDetails = it.userDetails
                updateUI(userDetails)
            }
        })

        viewModel.networkState().observe(this, Observer {
            when (it) {
                NetworkState.LOADING -> swipeRefresh.isRefreshing = true
                else -> swipeRefresh.isRefreshing = false
            }
        })
    }

    private fun updateUI(userDetails: UserDetails) {
        dashboard.totalBets.text = (userDetails.bets_processed.size + userDetails.bets_placed.size).toString()
//        dashboard.prevBet.betValue.text = userDetails.bets_processed[0].placed.toString()
//        dashboard.prevBet.raceName.text = userDetails.bets_processed[0].race_name
//        dashboard.prevBet.returnValue.text = userDetails.bets_processed[0].won.toString()
        dashboard.totalReturns.text =
            "${getString(R.string.currency)} ${userDetails.bets_processed.map { it.won }.sum()}"
        if (userDetails.bets_placed.isEmpty()) dashboard.activeBets.text = "No ${getString(R.string.active_bets)}"
        else dashboard.activeBets.text = "${userDetails.bets_placed.size} ${getString(R.string.active_bets)}"
        populateActiveBets(userDetails)
    }

    private fun populateActiveBets(userDetails: UserDetails) {
        activeBetsList.removeAllViews()
        for (bet: PlacedBet in userDetails.bets_placed) {
            val placedBetCard = layoutInflater.inflate(R.layout.bet_placed, null)
            placedBetCard.race.text = bet.race_name
            placedBetCard.amount.text = "${getString(R.string.currency)} ${bet.placed}"
            placedBetCard.horse.text = bet.horse_name
            activeBetsList.addView(placedBetCard)
        }
    }

    private fun setupButtons() {
//        loginToKioskBtn.setOnClickListener(this)
        kioskLoginBtn.setOnClickListener(this)
        dashboard.activeBets.setOnClickListener(this)
        dashboard.prevBets.setOnClickListener(this)
        statsBtn.setOnClickListener(this)
    }

    private fun getBluetoothPermission() {
        EasyPermissions.requestPermissions(
            PermissionRequest.Builder(
                this, RC_BLUETOOTH, Manifest.permission.BLUETOOTH,
                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH_ADMIN
            )
                .setRationale(R.string.camera_and_location_rationale)
                .setPositiveButtonText(R.string.rationale_ask_ok)
                .setNegativeButtonText(R.string.rationale_ask_cancel)
                .build()
        )
    }

    override fun onClick(v: View?) {
        when (v?.id) {
//            R.id.loginToKioskBtn -> mainViewModel.getQrCodeForKiosk()
            R.id.kioskLoginBtn -> startActivity(intentFor<KioskLoginActivity>())
            R.id.prevBets -> {
                startActivity(intentFor<PreviousBetsActivity>())
            }
            R.id.statsBtn -> startActivity(intentFor<StatisticsActivity>())
            R.id.activeBets -> toggleActiveBetsVisibility()
        }
    }

    override fun onRefresh() {
        viewModel.getUserDetails()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    private fun toggleActiveBetsVisibility() {
        if (activeBetsList.visibility == View.GONE) {
            activeBetsList.visibility = View.VISIBLE
            activeBets.setCompoundDrawablesWithIntrinsicBounds(null, null, getDrawable(R.drawable.expand_less), null)
        } else {
            activeBetsList.visibility = View.GONE
            activeBets.setCompoundDrawablesWithIntrinsicBounds(null, null, getDrawable(R.drawable.expand_more), null)
        }
    }
}
