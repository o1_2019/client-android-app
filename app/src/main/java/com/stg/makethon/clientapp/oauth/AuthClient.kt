package com.stg.makethon.clientapp.oauth

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.net.Uri
import com.stg.makethon.clientapp.oauth.Constants.STORE_NAME
import net.openid.appauth.*
import timber.log.Timber


class AuthClient private constructor(context: Context, val postAuthFlow: () -> Unit) :
    AuthorizationService.TokenResponseCallback {
    private val authConfig by lazy { Utils.loadConfig(context) }
    private val authService by lazy { createAuthService(context) }
    private val authState by lazy { AuthState(serviceConfiguration) }
    private val prefs by lazy { context.getSharedPreferences(STORE_NAME, MODE_PRIVATE) }
    private val serviceConfiguration by lazy {
        AuthorizationServiceConfiguration(Uri.parse(authConfig?.authUrl), Uri.parse(authConfig?.tokenUrl))
    }

    private var accessToken = prefs.getString(Constants.ACCESS_TOKEN, "")
    private var refreshToken = prefs.getString(Constants.REFRESH_TOKEN, "")
    private var tokenExpiryTime = prefs.getLong(Constants.EXPIRY_TIME, -1)

    companion object {
        private var instance: AuthClient? = null

        fun getClient(context: Context, postAuthFlow: () -> Unit) =
            instance ?: synchronized(this) {
                instance ?: AuthClient(context, postAuthFlow)
            }
    }

    var isAuthorized: Boolean = this.accessToken.isNotBlank()

    private val authRequest = AuthorizationRequest.Builder(
        serviceConfiguration,
        authConfig!!.clientId,
        ResponseTypeValues.CODE,
        Uri.parse(authConfig?.redirectUri)
    ).setScope(authConfig?.authorizationScope).build()

    init {
        // Check if token is already present and valid.
        // If token is present and valid, return token.
        // If token present but expired, fetch new token using refresh token
        Timber.d(accessToken)
        Timber.d(refreshToken)
        Timber.d(tokenExpiryTime.toString())
        Timber.d("Current Timestamp ${System.currentTimeMillis()}")
        if (!this.isAuthorized) {
            Timber.d("User not authorized. login flow")
        }
    }

    fun authenticate(callback: (Intent) -> Unit) {
        val authIntent = authService.getAuthorizationRequestIntent(authRequest)
        callback(authIntent)
    }

    fun exchangeCodeForToken(intent: Intent) {
        val resp = AuthorizationResponse.fromIntent(intent)
        val ex = AuthorizationException.fromIntent(intent)
        authState.update(resp, ex)
        if (resp != null)
            authService.performTokenRequest(resp.createTokenExchangeRequest(), this)
        else {
            Timber.e(ex)
        }
    }

    private fun createAuthService(context: Context): AuthorizationService {
        val appAuthConfig = AppAuthConfiguration.Builder()
            .setConnectionBuilder(CustomConnectionBuilder)
            .build()
        return AuthorizationService(context, appAuthConfig)
    }

    override fun onTokenRequestCompleted(response: TokenResponse?, ex: AuthorizationException?) {
        if (response != null) {
            Timber.d("Token request complete --------------------------------------------------------")
            Timber.d("Access Token: ${response.accessToken}")
            Timber.d("Access token expires in ${response.accessTokenExpirationTime}")
            Timber.d("Refresh Token: ${response.refreshToken}")
            storeTokens(response)
            authState.update(response, ex)
            postAuthFlow()
        } else {
            Timber.e(ex)
        }
    }

    private fun isTokenValid() = this.tokenExpiryTime > (System.currentTimeMillis() - 1000)

    fun getValidAccessToken(): String {
        if (!isTokenValid()) {
            Timber.d("Token expired. getting new token")
            refreshAccessToken()
        }
        Timber.d("Access token still valid")
        return this.accessToken
    }

    private fun refreshAccessToken() {
        val clientAuthentication = authState.clientAuthentication
        authService.performTokenRequest(authState.createTokenRefreshRequest(), clientAuthentication, this)
        this.accessToken = prefs.getString(Constants.ACCESS_TOKEN, "")
    }

    private fun storeTokens(response: TokenResponse) {
        prefs.edit().putString(Constants.ACCESS_TOKEN, response.accessToken).apply()
        prefs.edit().putString(Constants.REFRESH_TOKEN, response.refreshToken).apply()
        prefs.edit().putLong(Constants.EXPIRY_TIME, response.accessTokenExpirationTime!!).apply()
    }

    fun dispose() {
        if (authService != null) authService.dispose()
    }
}
