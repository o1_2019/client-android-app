package com.stg.makethon.clientapp.ui

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.awesome.shorty.AwesomeToast
import com.stg.makethon.clientapp.R
import com.stg.makethon.clientapp.api.NetworkState
import com.stg.makethon.clientapp.viewmodel.KioskLoginViewModel
import kotlinx.android.synthetic.main.activity_kiosk_login.*
import org.jetbrains.anko.imageBitmap
import org.jetbrains.anko.toast
import timber.log.Timber

class KioskLoginActivity : AppCompatActivity(), View.OnClickListener {
    private val viewModel: KioskLoginViewModel by lazy {
        ViewModelProviders.of(this).get(KioskLoginViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kiosk_login)
        setupViewModel()
        setupButtons()
    }

    private fun setupViewModel() {
        viewModel.resetLiveData()
        observeNetworkState()
        observeQrcodeResponse()
        observeKioskResponse()
//        observeBluetoothStatus()
    }

    private fun observeNetworkState() {
        viewModel.networkState().observe(this, Observer {
            when (it) {
                NetworkState.LOADING -> loginToKioskBtn.text = getString(R.string.fetching_msg)
                else -> loginToKioskBtn.text = getString(R.string.generate_identity)
            }
        })
    }

    private fun observeQrcodeResponse() {
        viewModel.qrcode().observe(this, Observer {
            if (it.isNotEmpty()) {
                val bitmap = BitmapFactory.decodeByteArray(it, 0, it.size)
                qrCodeForKiosk.imageBitmap = Bitmap.createBitmap(bitmap)
            } else {
                toast("No bytearray returned")
            }
        })
    }

    private fun observeKioskResponse() {
        viewModel.kioskNumber().observe(this, Observer {
            if (it.isNotBlank()) {
                AwesomeToast.success(
                    applicationContext,
                    "You are at Kiosk $it. Go ahead and place your bets!",
                    Toast.LENGTH_LONG
                ).show()
                finish()
            }
        })
    }

//    private fun observeBluetoothStatus() {
//        viewModel.bluetoothStatus().observe(this, Observer {
//            if (it) Timber.d("Bluetooth On")
//            else Timber.d("Bluetooth Off")
//        })
//    }

    private fun setupButtons() {
        loginToKioskBtn.setOnClickListener(this)
        kioskLoginMsg.visibility = View.VISIBLE
        qrCodeForKiosk.visibility = View.INVISIBLE
    }

    private fun loginToKiosk() {
        kioskLoginMsg.visibility = View.INVISIBLE
        qrCodeForKiosk.visibility = View.VISIBLE
        viewModel.getQrCodeForKiosk()
    }

    override fun onClick(v: View?) {
        Timber.d("Some button was clicked")
        when (v?.id) {
            R.id.loginToKioskBtn -> loginToKiosk()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.stop2FA()
    }
}
