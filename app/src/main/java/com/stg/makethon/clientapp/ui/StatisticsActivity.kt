package com.stg.makethon.clientapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.chip.ChipGroup
import com.stg.makethon.clientapp.R
import com.stg.makethon.clientapp.model.UserDetails
import com.stg.makethon.clientapp.ui.adapters.CustomPagerAdapter
import com.stg.makethon.clientapp.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_statistics.*
import timber.log.Timber

class StatisticsActivity : AppCompatActivity(), ChipGroup.OnCheckedChangeListener {
    lateinit var userDetails: UserDetails
    private val viewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistics)
        setupViewModel()
        viewModel.getUserDetails()
    }

    private fun setupViewModel() {
        viewModel.userDetails().observe(this, Observer {
            if (it != null) {
                Timber.d(it.userDetails.toString())
                userDetails = it.userDetails
                setupCharts(userDetails)
            }
        })
    }

    private fun setupCharts(userDetails: UserDetails) {
        chartGroup.setOnCheckedChangeListener(this)
        pieChartChip.isChecked = true
        chartPager.adapter = CustomPagerAdapter(supportFragmentManager, userDetails)
        chartPager.currentItem = 0
    }

    override fun onCheckedChanged(chipGroup: ChipGroup?, id: Int) {
        when (id) {
            R.id.pieChartChip -> chartPager.currentItem = 0
            R.id.lineChartChip -> chartPager.currentItem = 1
        }
    }
}
