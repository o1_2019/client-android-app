package com.stg.makethon.clientapp.oauth

object Constants {
    const val ACCESS_TOKEN = "accessToken"
    const val REFRESH_TOKEN = "refreshToken"
    const val EXPIRY_TIME = "expiryTime"
    const val STORE_NAME = "fairBet"
    const val UUID = "uuid"
}