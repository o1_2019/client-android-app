package com.stg.makethon.clientapp.api

import com.stg.makethon.clientapp.model.UserData
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface KioskLoginService {

    @POST("/api/login/mobile/qrcode")
    fun getQrCodeForKiosk(@Body userData: UserData): Call<ResponseBody>
}