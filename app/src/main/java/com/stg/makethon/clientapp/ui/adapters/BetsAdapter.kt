package com.stg.makethon.clientapp.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.stg.makethon.clientapp.R
import com.stg.makethon.clientapp.model.Bet
import com.stg.makethon.clientapp.ui.adapters.BetsAdapter.BetViewHolder

class BetsAdapter(private val context: Context, private var bets: ArrayList<Bet>, private val listener: (Bet) -> Unit) :
    RecyclerView.Adapter<BetViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BetViewHolder =
        BetViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.bet_card, parent, false))

    override fun getItemCount(): Int = bets.size

    override fun onBindViewHolder(holder: BetViewHolder, position: Int) = holder.bind(bets[position], listener)

    fun updateList(newBets: ArrayList<Bet>) {
        bets = newBets
    }

    class BetViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val horseName: TextView = view.findViewById(R.id.horseName)
        private val raceName: TextView = view.findViewById(R.id.raceName)
        private val date: TextView = view.findViewById(R.id.raceDate)
        private val betValue: TextView = view.findViewById(R.id.betValue)
        private val returns: TextView = view.findViewById(R.id.returnValue)
        private val card: MaterialCardView = view.findViewById(R.id.betCard)
        private val redColor = view.resources.getColor(R.color.red_btn_bg_color)
        private val greenColor = view.resources.getColor(R.color.color_light_green)

        fun bind(bet: Bet, listener: (Bet) -> Unit) {
            horseName.text = bet.horseName
            raceName.text = bet.raceName
            date.text = bet.date
            betValue.text = bet.betValue.toString()
            returns.text = bet.returns.toString()
            card.setOnClickListener { listener(bet) }
            if (bet.returns == 0.0) {
                card.setCardBackgroundColor(redColor)
            } else {
                card.setCardBackgroundColor(greenColor)
            }
        }
    }
}