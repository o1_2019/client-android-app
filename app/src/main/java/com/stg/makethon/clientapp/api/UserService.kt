package com.stg.makethon.clientapp.api

import com.stg.makethon.clientapp.model.UserDetailsEnv
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface UserService {

    @GET("/api/details/user")
    fun getUserDetails(@Query("userId") userId: String): Call<UserDetailsEnv>
}