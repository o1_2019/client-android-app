package com.stg.makethon.clientapp.oauth

import android.annotation.SuppressLint
import android.net.Uri
import net.openid.appauth.Preconditions
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import javax.net.ssl.*
import javax.security.cert.X509Certificate
import net.openid.appauth.connectivity.ConnectionBuilder
import java.net.HttpURLConnection
import java.net.URL


object CustomConnectionBuilder : ConnectionBuilder {
    private const val HTTP = "http"
    private const val HTTPS = "https"

    private var TRUSTING_CONTEXT: SSLContext? = null

    private val ANY_CERT_MANAGER = arrayOf<TrustManager>(object : X509TrustManager {
        override fun checkClientTrusted(chain: Array<out java.security.cert.X509Certificate>?, authType: String?) {
        }

        override fun checkServerTrusted(chain: Array<out java.security.cert.X509Certificate>?, authType: String?) {
        }

        override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        val acceptedIssuers: Array<X509Certificate>?
            get() = null

        @SuppressLint("TrustAllX509TrustManager")
        fun checkClientTrusted(x509Certificates: Array<X509Certificate>, s: String) {
        }

        @SuppressLint("TrustAllX509TrustManager")
        fun checkServerTrusted(x509Certificates: Array<X509Certificate>, s: String) {
        }
    })

    init {
        var sslContext: SSLContext?
        try {
            sslContext = SSLContext.getInstance("SSL")
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
            sslContext = null
        }

        var initializedContext: SSLContext? = null
        if (sslContext != null) {
            try {
                sslContext.init(null, ANY_CERT_MANAGER, java.security.SecureRandom())
                initializedContext = sslContext
            } catch (e: KeyManagementException) {
                e.printStackTrace()
            }

        }
        TRUSTING_CONTEXT = initializedContext
    }

    private val ANY_HOSTNAME_VERIFIER = object : HostnameVerifier {
        @SuppressLint("BadHostnameVerifier")
        override fun verify(hostname: String, session: SSLSession): Boolean {
            return true
        }
    }

    override fun openConnection(uri: Uri): HttpURLConnection {
        Preconditions.checkNotNull(uri, "url must not be null")
        Preconditions.checkArgument(
            HTTP == uri.scheme || HTTPS == uri.scheme,
            "scheme or uri must be http or https"
        )
        val connection = URL(uri.toString()).openConnection() as HttpURLConnection
        connection.instanceFollowRedirects = false

        if (connection is HttpsURLConnection && TRUSTING_CONTEXT != null) {
            val httpsConn = connection as HttpsURLConnection
            httpsConn.sslSocketFactory = TRUSTING_CONTEXT!!.socketFactory
            httpsConn.hostnameVerifier = ANY_HOSTNAME_VERIFIER
        }
        return connection
    }

}