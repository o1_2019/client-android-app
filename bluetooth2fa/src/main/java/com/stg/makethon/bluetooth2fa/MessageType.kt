package com.stg.makethon.bluetooth2fa

object MessageType {
    const val UUID = "uuid"
    const val TOKEN = "token"
    const val SUCCESS = "success"
}