package com.stg.makethon.clientapp.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.stg.makethon.bluetooth2fa.Bluetooth2FA
import com.stg.makethon.clientapp.R
import com.stg.makethon.clientapp.api.KioskLoginService
import com.stg.makethon.clientapp.api.NetworkState
import com.stg.makethon.clientapp.api.ServiceGenerator
import com.stg.makethon.clientapp.api.Urls
import com.stg.makethon.clientapp.model.UserData
import com.stg.makethon.clientapp.oauth.Constants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import java.util.concurrent.Executors

class KioskLoginViewModel(app: Application) : AndroidViewModel(app), Callback<ResponseBody> {
    private val qrCodeLiveData = MutableLiveData<ByteArray>()
    private val networkStateLiveData = MutableLiveData<NetworkState>()
    private val prefs by lazy { app.getSharedPreferences(Constants.STORE_NAME, Context.MODE_PRIVATE) }
//    private val bluetoothStatusLiveData = MutableLiveData<Boolean>().apply {
//        this.value = bluetoothAdapter.isEnabled
//        Timber.d("BLUETOOTH: ${bluetoothAdapter.isEnabled}")
//    }


    private val qrCodeService by lazy {
        ServiceGenerator.getInstance(Urls.BASE_URL).create(KioskLoginService::class.java)
    }

    private val executor by lazy { Executors.newSingleThreadExecutor() }
    private var tempUUID = UUID.randomUUID()
    private val appName = app.getString(R.string.app_name)

    fun getQrCodeForKiosk() {
//        tempUUID = UUID.randomUUID()
        Bluetooth2FA.start(appName, tempUUID, prefs.getString(Constants.ACCESS_TOKEN, "")!!)
        executor.execute { getQrCode() }
    }

    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
        Timber.e(t)
        networkStateLiveData.postValue(NetworkState(NetworkState.Status.FAILED, t.localizedMessage))
    }

    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
        if (response.isSuccessful) {
            networkStateLiveData.postValue(NetworkState.LOADED)
            qrCodeLiveData.postValue(response.body()?.bytes())
        }
    }

    private fun getQrCode() {
//        enableBluetooth()
        networkStateLiveData.postValue(NetworkState.LOADING)
        qrCodeService.getQrCodeForKiosk(
            UserData(tempUUID.toString(), getExpiryTime())
        ).enqueue(this)
    }

    fun stop2FA() {
        Bluetooth2FA.stop()
    }

    fun qrcode(): LiveData<ByteArray> = qrCodeLiveData
    fun networkState(): LiveData<NetworkState> = networkStateLiveData
    fun kioskNumber() = Bluetooth2FA.kioskNumber()
    fun status2FA() = Bluetooth2FA.status2FA()

    fun resetLiveData() {
        kioskNumber().value = ""
    }
//    fun bluetoothStatus(): LiveData<Boolean> = bluetoothStatusLiveData


    private fun getExpiryTime() = System.currentTimeMillis() + 20000
}