package com.stg.makethon.clientapp.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.stg.makethon.clientapp.R
import com.stg.makethon.clientapp.model.UserDetails
import com.stg.makethon.clientapp.ui.fragment.LineChartFragment
import com.stg.makethon.clientapp.ui.fragment.PieChartFragment

class CustomPagerAdapter(fragmentManager: FragmentManager, private val userDetails: UserDetails) :
    FragmentPagerAdapter(fragmentManager) {
    private val charts = listOf(R.layout.pie_chart, R.layout.line_chart)

    override fun getCount(): Int = charts.size

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> PieChartFragment.newInstance(userDetails)
            else -> LineChartFragment.newInstance(userDetails)
        }
    }
}