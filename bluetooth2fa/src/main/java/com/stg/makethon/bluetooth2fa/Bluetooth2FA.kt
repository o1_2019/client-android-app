package com.stg.makethon.bluetooth2fa

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.squareup.moshi.Moshi
import com.stg.makethon.bluetooth2fa.State.CONNECTED
import com.stg.makethon.bluetooth2fa.State.CONNECTING
import com.stg.makethon.bluetooth2fa.State.LISTEN
import com.stg.makethon.bluetooth2fa.State.NONE
import timber.log.Timber
import java.io.IOException
import java.util.*

object Bluetooth2FA {
    private val bluetoothAdapter by lazy { BluetoothAdapter.getDefaultAdapter() }
    private val kioskNumLiveData = MutableLiveData<String>()
    private val status2FALiveData = MutableLiveData<Boolean>().apply { value = false }
    private var bluetoothState = NONE
    private var acceptThread: AcceptThread? = null
    private var connectedThread: ConnectedThread? = null
    private val moshi by lazy { Moshi.Builder().build() }
    private val customAdapter by lazy {
        moshi.adapter(Message2FA::class.java)
    }

    private lateinit var appName: String
    private lateinit var uuid: UUID
    private lateinit var token: String

    @Synchronized
    fun start(appName: String, uuid: UUID, token: String) {
        Timber.d("Starting Bluetooth 2FA")
        Timber.d("Device ID is $uuid")
        this.appName = appName
        this.uuid = uuid
        this.token = token
        if (bluetoothAdapter.isEnabled) Timber.d("Bluetooth started")
        else {
            Timber.d("Starting Bluetooth....")
            bluetoothAdapter.enable()
        }

        while (!bluetoothAdapter.isEnabled) {
            Timber.d("Waiting for bluetooth to be enabled")
        }

        if (connectedThread != null) {
            connectedThread!!.cancel()
            connectedThread = null
        }

        if (acceptThread != null) {
            acceptThread!!.cancel()
            acceptThread = null
        }

        if (acceptThread == null) {
            acceptThread = AcceptThread()
            acceptThread!!.start()
        }
    }

    @Synchronized
    fun stop() {
        // Cancel any thread currently running a connection
        if (connectedThread != null) {
            connectedThread!!.cancel()
            connectedThread = null
        }

        // Cancel the accept thread because we only want to connect to one device
        if (acceptThread != null) {
            acceptThread!!.cancel()
            acceptThread = null
        }
        bluetoothState = NONE
        bluetoothAdapter.disable()
    }

    @Synchronized
    private fun initiate2FA(socket: BluetoothSocket, device: BluetoothDevice) {
        Timber.d("Connected to ${device.name}")

        // Cancel any thread currently running a connection
        if (connectedThread != null) {
            connectedThread!!.cancel()
            connectedThread = null
        }

        // Cancel the accept thread because we only want to connect to one device
        if (acceptThread != null) {
            acceptThread!!.cancel()
            acceptThread = null
        }

        // Start the thread to manage the connection and perform transmissions
        connectedThread = ConnectedThread(socket)
        connectedThread!!.start()
    }

    @Synchronized
    private fun complete2FA(resp: String) {
        stop()
        status2FALiveData.postValue(true)
        kioskNumLiveData.postValue(resp)
    }

    fun write(message: String) {
        var tmpThread: ConnectedThread?
        synchronized(this) {
            if (bluetoothState != CONNECTED && connectedThread != null) return
            else tmpThread = connectedThread
        }
        tmpThread?.write(message.toByteArray())
    }

    private class AcceptThread : Thread() {
        // The local server socket
        private val serverSocket: BluetoothServerSocket?

        init {
            var tmp: BluetoothServerSocket? = null

            // Create a new listening server socket
            try {
                tmp = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(
                    appName, uuid
                )
            } catch (e: IOException) {
                Timber.e(e, "Socket listen() failed")
            }

            serverSocket = tmp
            bluetoothState = LISTEN
        }

        override fun run() {
            name = AcceptThread::class.java.simpleName
            Timber.d("BEGIN mAcceptThread $this")

            var socket: BluetoothSocket?

            // Listen to the server socket if we're not connected
            while (bluetoothState != CONNECTED) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    Timber.d("Waiting for connection")
                    socket = serverSocket!!.accept()
                    Timber.d(" server socket got a connection request")
                } catch (e: IOException) {
                    Timber.e(e, "accept() failed")
                    break
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized(this) {
                        when (bluetoothState) {
                            LISTEN, CONNECTING ->
                                // Situation normal. Start the connected thread.
                                initiate2FA(socket, socket.remoteDevice)
                            NONE, CONNECTED ->
                                // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket.close()
                                } catch (e: IOException) {
                                    Timber.e(e, "Could not close unwanted socket")
                                }
                            else ->
                                Timber.d("ERROR STATE")
                        }
                    }
                }
            }
            Timber.i("END mAcceptThread")

        }

        fun cancel() {
            Timber.d("cancel $this")
            try {
                serverSocket!!.close()
            } catch (e: IOException) {
                Timber.e(e, "close() of server failed")
            }
        }
    }

    private class ConnectedThread(val bluetoothSocket: BluetoothSocket) : Thread() {
        val inputStream = bluetoothSocket.inputStream!!
        val outputStream = bluetoothSocket.outputStream!!

        init {
            bluetoothState = CONNECTED
        }

        override fun run() {
            val buffer = ByteArray(1024)

            while (bluetoothState == CONNECTED) {
                try {
                    val bytes = inputStream.read(buffer)
                    val msg = customAdapter.fromJson(String(buffer.take(bytes).toByteArray()))
                    Timber.d("Received Message: $msg")
                    if (msg != null)
                        parseMessage(msg)
                } catch (e: IOException) {
                    Timber.e(e, "disconnected")
                    connectionLost()
                    break
                }
            }
        }

        fun write(buffer: ByteArray) {
            try {
                outputStream.write(buffer)
                Timber.d("Sent Message: ${String(buffer)}")
            } catch (e: IOException) {
                Timber.e(e, "disconnected")
            }
        }

        fun cancel() {
            Timber.d("cancel $this")
            try {
                bluetoothSocket.close()
            } catch (e: IOException) {
                Timber.e(e, "close() of connection failed")
            }
        }

        fun parseMessage(msg: Message2FA) {
            when (msg.type) {
                MessageType.UUID -> validateUUID(msg.data)
                MessageType.SUCCESS -> complete2FA(msg.data)
                else -> Timber.d("Unsupported command: ${msg.type}")
            }
        }

        fun validateUUID(uuidString: String): Boolean {
            return if (uuidString == uuid.toString()) {
                sendToken()
                true
            } else
                false
        }

        fun sendToken() {
            val message = Message2FA(MessageType.TOKEN, token)
            write(customAdapter.toJson(message).toByteArray())
        }
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private fun connectionLost() {
        bluetoothState = NONE
        // Start the service over to restart listening mode
        Bluetooth2FA.start(appName, uuid, token)
    }

    fun kioskNumber() = kioskNumLiveData
    fun status2FA(): LiveData<Boolean> = status2FALiveData
}