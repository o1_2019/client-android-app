package com.stg.makethon.clientapp.oauth

data class AuthConfig(
    val clientId: String,
    val redirectUri: String,
    val authorizationScope: String,
    val httpsRequired: Boolean,
    val authUrl: String,
    val tokenUrl: String,
    val discoveryUri: String
)