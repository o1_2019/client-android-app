package com.stg.makethon.clientapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.stg.makethon.clientapp.R
import com.stg.makethon.clientapp.oauth.AuthClient
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import timber.log.Timber


class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var authClient: AuthClient
    private val RC_AUTH = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        // Auth Flow -------------------------------------------------------------
        authClient = AuthClient.getClient(this) { continueFlow() }
        // -----------------------------------------------------------------------
        if (authClient.isAuthorized) {
            Timber.d("User already authenticated. Continuing")
            continueFlow()
        } else {
            setupButtons()
        }
    }

    private fun setupButtons() {
        loginBtn.visibility = View.VISIBLE
        loginBtn.setOnClickListener(this)
    }

    private fun startAuth() {
        authClient.authenticate { intent -> startActivityForResult(intent, RC_AUTH) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_AUTH) {
            if (data != null) {
                authClient.exchangeCodeForToken(data)
            } else
                toast("Error logging in. Please try again after some time.")
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.loginBtn -> startAuth()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("Disposing auth service")
        authClient.dispose()
    }

    private fun continueFlow() {
        startActivity(intentFor<MainActivity>())
        finish()
    }
}
