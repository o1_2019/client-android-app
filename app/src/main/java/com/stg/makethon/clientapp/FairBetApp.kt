package com.stg.makethon.clientapp

import android.app.Application
import timber.log.Timber

class FairBetApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}