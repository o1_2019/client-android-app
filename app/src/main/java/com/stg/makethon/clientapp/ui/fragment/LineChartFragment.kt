package com.stg.makethon.clientapp.ui.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.data.Set
import com.anychart.enums.TooltipPositionMode
import com.stg.makethon.clientapp.R
import com.stg.makethon.clientapp.model.ProcessedBet
import com.stg.makethon.clientapp.model.UserDetails

/**
 * A simple [Fragment] subclass.
 *
 */
class LineChartFragment : Fragment() {
    private lateinit var userDetails: UserDetails

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_line_chart, container, false)
        setupChart(view)
        return view
    }

    companion object {
        fun newInstance(ud: UserDetails): LineChartFragment {
            val lineChartFragment = LineChartFragment()
            lineChartFragment.userDetails = ud
            return lineChartFragment
        }
    }

    private fun setupChart(view: View) {
        val line = AnyChart.line()
        line.animation(true)
        line.crosshair().enabled(true)
        line.tooltip().positionMode(TooltipPositionMode.POINT)
        val dataPoints = ArrayList<DataEntry>()
        dataPoints.add(ValueDataEntry("Start", 0))
        var i = 1
        for (b: ProcessedBet in userDetails.bets_processed) {
            val value = if (b.success) b.won else -b.placed
            dataPoints.add(ValueDataEntry("Race $i", value))
            i += 1
        }
        val set = Set.instantiate()
        set.data(dataPoints)
        line.addSeries(set)
        val chart = view.findViewById<AnyChartView>(R.id.lineChart)
        chart.setChart(line)

    }


}
