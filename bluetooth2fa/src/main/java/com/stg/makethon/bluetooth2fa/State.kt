package com.stg.makethon.bluetooth2fa

object State {
    const val NONE = 0       // we're doing nothing
    const val LISTEN = 1     // now listening for incoming connections
    const val CONNECTING = 2 // now initiating an outgoing connection
    const val CONNECTED = 3  // now connected to a remote device
}