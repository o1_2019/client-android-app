package com.stg.makethon.bluetooth2fa

data class Message2FA(val type: String, val data: String)